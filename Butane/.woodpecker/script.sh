#!/bin/sh

# loop through all butane files in the current directory
for i in *.bu ; do
  # extract filename without extension
  name=${i%.bu}

  # use butane to convert the YAML file into an Ignition configuration
  butane -ps ${i} -o ${name}.ign

  if [ $? -eq 0 ]; then
    echo "Converted ${i} to ${name}.ign successfully"
  else
    echo "Error converting ${i}"
  fi
done
