# Woodpecker examples

Example Woodpecker pipelines for various use cases.
Pull requests are welcome!

| Link                                                                         | Language    | Build System        | Comments                                                                                                                                                                 |
| :--------------------------------------------------------------------------- | :---------- | :------------------ | :----------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [Butane/.woodpecker/butane.yaml](Butane/.woodpecker/butane.yaml)             | Bash        | ---                 | Translate Butane configs into Ignition configs with a simple bash script.                                                                                                |
| [C/.woodpecker/make.yaml](C/.woodpecker/make.yaml)                           | C           | Make                | Simple ci for building a Make based C project                                                                                                                            |
| [C/.woodpecker/meson.yaml](C/.woodpecker/meson.yaml)                         | C/C++       | meson               | CI for meson-based projects.                                                                                                                                             |
| [C/.woodpecker/meson-android.yaml](C/.woodpecker/meson-android.yaml)         | C/C++       | meson + Android NDK | CI for meson-based projects cross-compiling to Android NDK.                                                                                                              |
| [Docker/.woodpecker/kaniko.yaml](Docker/.woodpecker/kaniko.yaml)             | Dockerfile  | [Kaniko][1]         | Minimalistic CI pipeline with clear instructions to push a Docker image                                                                                                  |
| [Docker/.woodpecker/buildx.yaml](Docker/.woodpecker/buildx.yaml)             | Dockerfile  | [buildx][2]         | Build and publish Docker images for multiple architectures on codeberg                                                                                                   |
| [Git/.woodpecker.yaml](Git/.woodpecker.yaml)                                 | ---         | ---                 | CI step to automatic synchronization with an upstream repository.                                                                                                        |
| [Golang/.woodpecker/build.yaml](Golang/.woodpecker/build.yaml)               | golang      | golang              | Simple ci for building and test a Go project                                                                                                                             |
| [Golang/.woodpecker/build-docker.yaml](Golang/.woodpecker/build-docker.yaml) | golang      | golang / kaniko     | CI to build golang project and build various docker container and publish them on DockerHub                                                                              |
| [Hugo/.woodpecker.yaml](Hugo/.woodpecker.yaml)                               | Markdown    | Hugo                | CI step to build static website files and publish them to Codeberg Pages with Hugo                                                                                       |
| [Idris/.woodpecker.yaml](Idris/.woodpecker.yaml)                             | Idris       | Idris               | Typechecks your Idris project and ensures documentation can be generated without errors                                                                                  |
| [Jekyll/.woodpecker/jekyll.yaml](Jekyll/.woodpecker/jekyll.yaml)             | Markdown    | Jekyll              | CI step to build static website files and publish them to Codeberg Pages using Jekyll                                                                                    |
| [Julia/.woodpecker.yaml](Julia/.woodpecker.yaml)                             | Julia       | Pkg.jl              | Standard CI pipeline to test Julia packages.                                                                                                                             |
| [KiCad/.woodpecker.yaml](KiCad/.woodpecker.yaml)                             | ---         | KiBot               | Building gerber files, ibom and a pdf schematic as an example for KiBot use                                                                                              |
| [Mdbook/.woodpecker.yaml](Mdbook/.woodpecker.yaml)                           | Markdown    | mdbook              | CI step to build static website files and publish them to Codeberg Pages with mdbook                                                                                     |
| [NodeJS/.woodpecker.yaml](NodeJS/.woodpecker.yaml)                           | JavaScript  | NodeJS              | CI to build static websites files and publish them to Codeberg Pages with 11ty                                                                                           |
| [PHP/.woodpecker.yaml](PHP/.woodpecker.yaml)                                 | PHP         | Bash                | CI step lint PHP files and report on errors                                                                                                                              |
| [Python/.woodpecker/mkdocs.yaml](Python/.woodpecker/mkdocs.yaml)             | Markdown    | MkDocs              | Static Site Generator mkdocs example                                                                                                                                     |
| [Python/.woodpecker/code-quality.yaml](Python/.woodpecker/code-quality.yaml) | Python      | Python venv         | Standard CI pipeline to test Python packages code on multiple Python distributions. For details check [serial-sphinx](https://codeberg.org/sail.black/serial-sphinx.git) |
| [Python/.woodpecker/pdoc.yaml](Python/.woodpecker/pdoc.yaml)                 | Python      | Python venv         | Build API documentation with pdoc & publish files on a pages branch                                                                                                      |
| [R/.woodpecker.yaml](Typst/.woodpecker.yaml)                                 | R           | R                   | Installs R package dependencies and runs R CMD check                                                                                                                     |
| [REUSE/.woodpecker.yaml](REUSE/.woodpecker.yaml)                             | ---         | ---                 | Lints a project for REUSE licensing compliance. For details, check [reuse-tool](https://reuse.software/) and [SPDX](https://spdx.dev/)                                   |
| [Rust/.woodpecker.yaml](Rust/.woodpecker.yaml)                               | Rust        | cargo               | Simple CI pipeline to run cargo commands                                                                                                                                 |
| [StandardML/.woodpecker.yaml](StandardML/.woodpecker.yaml)                   | Standard ML | sml                 | Simple CI pipeline to run SML files                                                                                                                                      |
| [Tectonic/.woodpecker.yaml](Tectonic/.woodpecker.yaml)                       | LaTeX       | Tectonic            | Builds a TeX file and pushes the resulting PDF file to a given repository.                                                                                               |
| [Typst/.woodpecker.yaml](Typst/.woodpecker.yaml)                             | Typst       | Typst               | Builds a Typst file and pushes the resulting PDF file to a given repository.                                                                                             |

## More examples from Codeberg

[Codeberg repos with Woodpecker YAML files](https://codeberg.org/explore/repos?q=woodpecker-ci&topic=1)

[1]: https://github.com/GoogleContainerTools/kaniko
[2]: https://codeberg.org/woodpecker-plugins/plugin-docker-buildx

## How to add a new example

1. Create a new subdirectory with a descriptive name. Capitalize the first letter.
1. If you only add one file: name it `.woodpecker.yaml`. If you want to add multiple files: add a `.woodpecker` directory and use descriptive names for the individual yaml files.
1. Add your example to the README table while respecting the alphabetical order.
1. Add your new subdirectory to the "woodpecker-cli" step in `.woodpecker.yaml` so it is getting linted over time.
